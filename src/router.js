import VueRouter from 'vue-router'
import People from './components/People/People.vue'
import Person from './components/Person/Person.vue'
import NotFound from './components/NotFound/NotFound.vue'

const routes = [
    {
        path: '/people',
        component: People,
        alias: '/'
    },
    {
        path: '/person/:id',
        component: Person
    },
    {
        path: '*',
        component: NotFound
    }
]

const router = new VueRouter({ routes })

export default router