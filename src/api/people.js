import { genereateId } from "../utils/generate-id";

const BASE_URL = 'https://randomuser.me/api/';

export function fetchPeople() {
    return fetch(`${ BASE_URL }/?seed=d30ada47cf509b86&results=20`)
        .then(responseToJson)
        .then(json => {
            return json.results
        })
        .then(people => {
            return people.map(person => ({
                ...person,
                id: genereateId()
            }))
        })
}

function responseToJson(response) {
    return response.json()
}